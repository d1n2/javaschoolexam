package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.size() == 0) {
            return true;
        }
        int countEqObj = 0;
        int k = 0;
        boolean result = false;
        for (int i = 0; i < x.size(); i++) {
            if (y.contains(x.get(i))) {
                for (int j = k; j < y.size(); j++) {
                    if (x.get(i).equals(y.get(j))) {
                        countEqObj++;
                        k = j + 1;
                        if (x.size() == countEqObj) {
                            return true;
                        }
                        break;
                    }
                }
            } else {
                result = false;
            }
        }
        return result;
    }
}

