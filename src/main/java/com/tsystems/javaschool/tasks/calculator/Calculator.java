package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
//
    public String evaluate(String statement) {
        LinkedList<Double> linkedList;
        try {
            String stringMath = statement.replaceAll(" ", "");
            linkedList = new LinkedList<Double>();
            LinkedList<Character> listOperators = new LinkedList<Character>();
            for (int i = 0; i < stringMath.length(); i++) {
                char c = stringMath.charAt(i);
                if (c == '(') {
                    listOperators.add('(');
                } else if (c == ')') {
                    while (listOperators.getLast() != '(') {
                        mathOp(linkedList, listOperators.removeLast());
                    }
                    listOperators.removeLast();
                } else if (isOp(c)) {
                    while (!listOperators.isEmpty() && priorOp(listOperators.getLast()) >= priorOp(c)) {
                        mathOp(linkedList, listOperators.removeLast());
                    }
                    listOperators.add(c);
                } else if (Character.isDigit(c)) {
                    String stringOperand = "";
                    while (i < stringMath.length() && (stringMath.charAt(i) == '.' || Character.isDigit(stringMath.charAt(i))
                            || stringMath.charAt(i) == ',')) {
                        if (stringMath.charAt(i) == ',') {
                            return null;
                        }
                        stringOperand += stringMath.charAt(i++);
                    }
                    --i;
                    try {
                        linkedList.add(Double.parseDouble(stringOperand));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                }
            }
            while (!listOperators.isEmpty()) {
                mathOp(linkedList, listOperators.removeLast());
            }
        } catch (Exception e) {
            return null;
        }
        try {
            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
            numberFormat.setMaximumFractionDigits(4);
            numberFormat.setMinimumFractionDigits(0);
            return numberFormat.format(linkedList.get(0));
        } catch (Exception e) {
            return null;
        }
    }

    public void mathOp(LinkedList<Double> linkedList, char charOp) throws Exception {
        double first = linkedList.removeLast();
        double second = linkedList.removeLast();
        switch (charOp) {
            case '+':
                linkedList.add(second + first);
                break;
            case '-':
                linkedList.add(second - first);
                break;
            case '*':
                linkedList.add(second * first);
                break;
            case '/':
                if (first == 0) {
                    throw new ArithmeticException();
                }
                linkedList.add(second / first);
                break;
            default:
                System.out.println("Неверное значение параметра operator");
        }
    }

    public boolean isOp(char charOp) {
        return charOp == '+' || charOp == '-' || charOp == '*' || charOp == '/';
    }

    public int priorOp(char charOp) {
        if (charOp == '*' || charOp == '/') {
            return 1;
        } else if (charOp == '+' || charOp == '-') {
            return 0;
        } else {
            return -1;
        }
    }
}





