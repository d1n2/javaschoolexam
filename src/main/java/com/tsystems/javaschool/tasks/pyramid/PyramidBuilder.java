package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        int result[][] = new int[0][0];

        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        if (inputNumbers.size() == 3 || inputNumbers.size() == 6 || inputNumbers.size() == 10 ||
                inputNumbers.size() == 15 || inputNumbers.size() == 21) {

            Collections.sort(inputNumbers);

            if (inputNumbers.size() == 3) {
                result = new int[2][3];
                result[0][1] = inputNumbers.get(0);
                result[1][0] = inputNumbers.get(1);
                result[1][2] = inputNumbers.get(2);
            }

            if (inputNumbers.size() == 6) {
                result = new int[3][5];
                result[0][2] = inputNumbers.get(0);
                result[1][1] = inputNumbers.get(1);
                result[1][3] = inputNumbers.get(2);
                result[2][0] = inputNumbers.get(3);
                result[2][2] = inputNumbers.get(4);
                result[2][4] = inputNumbers.get(5);
            }

            if (inputNumbers.size() == 10) {
                result = new int[4][7];
                result[0][3] = inputNumbers.get(0);
                result[1][2] = inputNumbers.get(1);
                result[1][4] = inputNumbers.get(2);
                result[2][1] = inputNumbers.get(3);
                result[2][3] = inputNumbers.get(4);
                result[2][5] = inputNumbers.get(5);
                result[3][0] = inputNumbers.get(6);
                result[3][2] = inputNumbers.get(7);
                result[3][4] = inputNumbers.get(8);
                result[3][6] = inputNumbers.get(9);
            }

            if (inputNumbers.size() == 15) {
                result = new int[5][9];
                result[0][4] = inputNumbers.get(0);
                result[1][3] = inputNumbers.get(1);
                result[1][5] = inputNumbers.get(2);
                result[2][2] = inputNumbers.get(3);
                result[2][4] = inputNumbers.get(4);
                result[2][6] = inputNumbers.get(5);
                result[3][1] = inputNumbers.get(6);
                result[3][3] = inputNumbers.get(7);
                result[3][5] = inputNumbers.get(8);
                result[3][7] = inputNumbers.get(9);
                result[4][0] = inputNumbers.get(10);
                result[4][2] = inputNumbers.get(11);
                result[4][4] = inputNumbers.get(12);
                result[4][6] = inputNumbers.get(13);
                result[4][8] = inputNumbers.get(14);
            }

            if (inputNumbers.size() == 21) {
                result = new int[6][11];
                result[0][5] = inputNumbers.get(0);
                result[1][4] = inputNumbers.get(1);
                result[1][6] = inputNumbers.get(2);
                result[2][3] = inputNumbers.get(3);
                result[2][5] = inputNumbers.get(4);
                result[2][7] = inputNumbers.get(5);
                result[3][2] = inputNumbers.get(6);
                result[3][4] = inputNumbers.get(7);
                result[3][6] = inputNumbers.get(8);
                result[3][8] = inputNumbers.get(9);
                result[4][1] = inputNumbers.get(10);
                result[4][3] = inputNumbers.get(11);
                result[4][5] = inputNumbers.get(12);
                result[4][7] = inputNumbers.get(13);
                result[4][9] = inputNumbers.get(14);
                result[5][0] = inputNumbers.get(15);
                result[5][2] = inputNumbers.get(16);
                result[5][4] = inputNumbers.get(17);
                result[5][6] = inputNumbers.get(18);
                result[5][8] = inputNumbers.get(19);
                result[5][10] = inputNumbers.get(20);
            }
            } else {
                throw new CannotBuildPyramidException();
            }
        return result;
        }
    }


